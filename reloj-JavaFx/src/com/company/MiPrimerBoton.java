package com.company;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.stage.Stage;

import java.awt.*;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class MiPrimerBoton extends Application {

    public void start(Stage primaryStage){

        /*

        Button button = new Button("Refresh");

        Label timezoneLondon = new Label("Europe/London");
        Label horaLondon = new Label(getLondonHour());

        VBox vBoxLondon = new VBox(timezoneLondon,horaLondon);
        vBoxLondon.setAlignment(Pos.CENTER);
        vBoxLondon.setMinHeight(150);
        vBoxLondon.setMaxWidth(200);

        Label timezoneMadrid = new Label("Europe/Madrid");
        Label horaMadrid = new Label(getMadridHour());

        VBox vBoxMadrid = new VBox(timezoneMadrid,horaMadrid);
        vBoxMadrid.setAlignment(Pos.CENTER);
        vBoxMadrid.setMinHeight(150);
        vBoxMadrid.setMaxWidth(200);

        HBox hBox = new HBox(vBoxLondon,vBoxMadrid);

        BorderPane borderPane = new BorderPane();
        borderPane.setCenter(hBox);
        borderPane.setBottom(button);

        */

        clockPane clockPane = new clockPane("Europe/London");

        Scene scene = new Scene(clockPane, 300, 300);
        primaryStage.setTitle("MyJavaFX");
        primaryStage.setScene(scene);
        primaryStage.show();

    }


    public String getLondonHour(){

        LocalDateTime londonDateHour = LocalDateTime.now(ZoneId.of("Europe/London"));

        int londonHour = londonDateHour.getHour();
        int londonMinute = londonDateHour.getMinute();
        int londonSeconds = londonDateHour.getSecond();

        String hhmmss=londonHour+":"+londonMinute+":"+londonSeconds;

        return hhmmss;

    }

    public String getMadridHour(){

        LocalDateTime londonDateHour = LocalDateTime.now(ZoneId.of("Europe/Madrid"));

        int londonHour = londonDateHour.getHour();
        int londonMinute = londonDateHour.getMinute();
        int londonSeconds = londonDateHour.getSecond();

        String hhmmss=londonHour+":"+londonMinute+":"+londonSeconds;

        return hhmmss;

    }

}
