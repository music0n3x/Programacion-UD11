package com.company;

import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

import java.time.LocalDateTime;
import java.time.ZoneId;

public class clockPane extends VBox{

    private Label timeZone;
    private Label hour;

    public clockPane(Label zoneID){

        this.timeZone=zoneID;
        this.hour=calculateHourFromZoneID();

    }

    public Label calculateHourFromZoneID(){

        LocalDateTime dateAndHour = LocalDateTime.now(ZoneId.of(this.timeZone));

        int Hour = dateAndHour.getHour();
        int Minute = dateAndHour.getMinute();
        int Seconds = dateAndHour.getSecond();

        String hhmmss=Hour+":"+Minute+":"+Seconds;

        Label label=hhmmss;

        return hhmmss;

    }

    public void updateVBox(){

        this.hour=calculateHourFromZoneID();

    }


    public void createVBox(){



    }

}
