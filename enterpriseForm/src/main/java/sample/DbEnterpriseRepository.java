package sample;


import java.sql.*;
import java.time.LocalDateTime;

//import static sample.PasswordHelper.generateSha1;

public class DbEnterpriseRepository implements EnterpriseRepository{

    private MySQLConnection connection;
    private int pageSize;

    DbEnterpriseRepository(){
        this.connection=new MySQLConnection("localhost", "crm_db", "root","micasabonita");
    }

    @Override
    public boolean save(Enterprise enterprise) {

        if (enterprise.getId() != null){
            return this.update(enterprise);
        }

        return this.insertAlex(enterprise);

    }

    @Override
    public Enterprise findById(int id) {

        Connection connection = this.connection.getConnection();
        String SQLQuery = "SELECT * FROM Enterprise WHERE id="+id;

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQLQuery);

            if (resultSet.next()) {
                Integer id2 = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String address = resultSet.getString("address");
                String city = resultSet.getString("city");
                String province = resultSet.getString("province");
                String country = resultSet.getString("country");
                String locale = resultSet.getString("locale");
                LocalDateTime createdOn = resultSet.getTimestamp("createdOn").toLocalDateTime();
                String nif = resultSet.getString("nif");
                int status = resultSet.getInt("status");

                return new Enterprise(id2, name, address, city, province, country, locale, createdOn, nif, status);
            }
        return null;

        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }


    }

    public Enterprise findByNif(String nif){

        Connection connection = this.connection.getConnection();
        String sql = "SELECT * FROM Enterprise WHERE nif = '"+nif+"'";

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            if (resultSet.next()) {
                Integer id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String address = resultSet.getString("address");
                String city = resultSet.getString("city");
                String province = resultSet.getString("province");
                String country = resultSet.getString("country");
                String locale = resultSet.getString("locale");
                LocalDateTime createdOn = resultSet.getTimestamp("createdOn").toLocalDateTime();
                String nif2 = resultSet.getString("nif");
                int status = resultSet.getInt("status");

                return new Enterprise(id, name, address, city, province, country, locale, createdOn, nif2, status);
            }
            return null;

        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }

    }
    public Enterprise findByNif2(String nif){

        Connection connection = this.connection.getConnection();
        String sql = "SELECT * FROM Enterprise WHERE nif = '"+nif+"'";

        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                Integer id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String address = resultSet.getString("address");
                String city = resultSet.getString("city");
                String province = resultSet.getString("province");
                String country = resultSet.getString("country");
                String locale = resultSet.getString("locale");
                LocalDateTime createdOn = resultSet.getTimestamp("createdOn").toLocalDateTime();
                String nif2 = resultSet.getString("nif");
                int status = resultSet.getInt("status");

                return new Enterprise(id, name, address, city, province, country, locale, createdOn, nif2, status);
            }
            return null;

        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }

    }

    @Override
    public Enterprise getById(int id) throws EnterpriseNotFoundException{

        Enterprise enterprise = findById(id);

        if (enterprise != null){
            return enterprise;
        }

        throw new EnterpriseNotFoundException();

    }

    public boolean insert(Enterprise enterprise){

        String insertValues = "'"+enterprise.getId()+"','"+enterprise.getName()+"','"+enterprise.getAddress()+"','"+enterprise.getCity()+"','"+enterprise.getProvince()+"','"+enterprise.getCountry()+"','"+enterprise.getLocale()+"','"+String.valueOf(enterprise.getCreatedOn())+"','"+enterprise.getNif()+"','"+enterprise.getStatus()+"'";

        String SQLQuery ="INSERT INTO Enterprise (name,address,city,province,country,locale,createdOn,nif,status) VALUES ("+insertValues+")";

        Connection connection = this.connection.getConnection();
        try {
            Statement statement = connection.createStatement();
            int resultSet = statement.executeUpdate(SQLQuery);
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        return true;

    }
/*
    public void updatePasswords(){

        Connection connection = this.connection.getConnection();
        try {

            Statement statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = statement.executeQuery("SELECT * FROM User");
            while (rs.next()) {
                String firstname = rs.getString("firstName");
                String lastname = rs.getString("lastName");
                String concatenatedName = firstname+"_"+lastname;
                rs.updateString("password", generateSha1(concatenatedName));
                rs.updateRow();
            }

        }catch (SQLException e){
            e.printStackTrace();

        }

    }
*/
    public boolean insertAlex(Enterprise enterprise){

        String insertValues = "'"+enterprise.getName()+"','"+enterprise.getAddress()+"','"+enterprise.getCity()+"','"+enterprise.getProvince()+"','"+enterprise.getCountry()+"','"+enterprise.getLocale()+"','"+String.valueOf(enterprise.getCreatedOn())+"','"+enterprise.getNif()+"','"+enterprise.getStatus()+"'";

        String sql ="INSERT INTO Enterprise (name,address,city,province,country,locale,createdOn,nif,status) VALUES ("+insertValues+")";

        Connection connection =this.connection.getConnection();

        try {

            Statement statement = connection.createStatement();
            statement.executeUpdate(sql,Statement.RETURN_GENERATED_KEYS);
            ResultSet resultSet = statement.getGeneratedKeys();
            if (resultSet.next()){
                 int idEnterprise = resultSet.getInt(1);
                enterprise.setId(idEnterprise);
                return true;
            }


        }catch (SQLException e){
            e.printStackTrace();

        }
        return false;
    }
    public boolean update(Enterprise enterprise){

        int id = enterprise.getId();
        String name = enterprise.getName();
        String address = enterprise.getAddress();
        String city = enterprise.getCity();
        String province = enterprise.getProvince();
        String country = enterprise.getCountry();
        String locale = enterprise.getLocale();
        String createdOn = String.valueOf(enterprise.getCreatedOn());
        String nif = enterprise.getNif();
        int status = enterprise.getStatus();

        Connection connection = this.connection.getConnection();

        try {
            Statement statement = connection.createStatement();
            int resultSet = statement.executeUpdate("UPDATE Enterprise SET name='"+name+"',address='"+address+"',city='"+city+"',province='"+province+"',country='"+country+"',locale='"+locale+"',nif='"+nif+"'  WHERE id="+id);
            return true;
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
            return false;
        }

    }



}