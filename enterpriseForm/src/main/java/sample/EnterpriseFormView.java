package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

import java.util.Arrays;

public class EnterpriseFormView extends GridPane {

    private Label idLabel;
    private TextField idTextField;
    private Label enterpriseNameLabel;
    private TextFieldErrorLabel enterpriseNameTextField;
    private Label nifLabel;
    private TextFieldErrorLabel nifTextField;
    private Label addressLabel;
    private TextFieldErrorLabel addressTextField;
    private Label cityLabel;
    private TextFieldErrorLabel cityTextField;
    private Label provinceLabel;
    private TextFieldErrorLabel provinceTextField;
    private Label countryLabel;
    private ChoiceBox<String> countryChoiceBox;
    private Label localeLabel;
    private ChoiceBox<String> localeChoiceBox;
    private Label statusLabel;
    private CheckBox statusCheckbox;
    private EventHandler<MouseEvent> formEventHandler = new FormEnterpriseEventHandler();

    EnterpriseFormView(){

        this.initView();

    }

    private void initView(){

        Text formTitle = new Text("Enterprise Form");
        formTitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        this.add(formTitle, 0, 0, 2, 1);

        /** Tipo textfield**/
        this.idLabel = new Label("ID");
        this.idTextField = new TextField();

        this.enterpriseNameLabel = new Label("Name:");
        this.enterpriseNameLabel.setAlignment(Pos.CENTER);
        this.enterpriseNameTextField = new TextFieldErrorLabel();


        this.nifLabel = new Label("Nif:");
        this.nifTextField = new TextFieldErrorLabel();

        this.addressLabel = new Label("Address:");
        this.addressTextField = new TextFieldErrorLabel();

        this.cityLabel = new Label("City:");
        this.cityTextField = new TextFieldErrorLabel();

        this.provinceLabel = new Label("Province:");
        this.provinceTextField = new TextFieldErrorLabel();

        this.add(this.idLabel,1,1);
        this.add(this.idTextField,2,1);
        this.add(this.enterpriseNameLabel,1,2);
        this.add(this.enterpriseNameTextField,2,2);
        this.add(this.nifLabel,1,3);
        this.add(this.nifTextField,2,3);
        this.add(this.addressLabel,1,4);
        this.add(this.addressTextField,2,4);
        this.add(this.cityLabel,1,5);
        this.add(this.cityTextField,2,5);
        this.add(this.provinceLabel,1,6);
        this.add(this.provinceTextField,2,6);


        /** Tipo choicebox**/
        this.countryLabel = new Label("Country:");
        this.countryChoiceBox = new ChoiceBox<>(getValidCountries());
        this.countryChoiceBox.setValue("es");

        this.localeLabel = new Label("Locale:");
        this.localeChoiceBox = new ChoiceBox<>(getValidLocales());
        this.localeChoiceBox.setValue("es_ES");

        /** Tipo checkbox**/
        this.statusLabel = new Label("Status:");
        this.statusCheckbox = new CheckBox();
        this.statusCheckbox.setSelected(true);

        Button saveButton = new Button("Save");
        this.add(saveButton, 0, 10, 2, 2);
        saveButton.setId("save-bt");
        saveButton.addEventHandler(MouseEvent.MOUSE_CLICKED ,this.formEventHandler);

        /** Crear componentes y añadirlos al panel */


        this.add(this.countryLabel,1,7);
        this.add(this.countryChoiceBox,2,7);
        this.add(this.localeLabel,1,8);
        this.add(this.localeChoiceBox,2,8);
        this.add(this.statusLabel,1,9);
        this.add(this.statusCheckbox,2,9);

    }

    private ObservableList<String> getValidLocales() {

        /**
         * Devolver un observable con las opciones que deben aparecer en el checkbox
         */

        return FXCollections.observableArrayList(Arrays.asList("es_ES", "gb_GB", "us_US"));



    }

    private ObservableList<String> getValidCountries() {

        return FXCollections.observableArrayList(Arrays.asList("es", "gb", "us"));

    }

    class FormEnterpriseEventHandler implements EventHandler<MouseEvent>{
        @Override
        public void handle(MouseEvent event) {

            Validator validator = new Validator();

            DbEnterpriseRepository dbEnterpriseRepository=new DbEnterpriseRepository();

            String controlId = ((Control) event.getSource()).getId();

            if (controlId.equals("save-bt")) {

                /**
                 * Crear un objeto de tipo empresa
                 * y mostrar por pantalla los datos de la empresa
                 */

                Enterprise enterprise = new Enterprise(enterpriseNameTextField.getText(),addressTextField.getText(),cityTextField.getText(),provinceTextField.getText(),countryChoiceBox.getValue(),localeChoiceBox.getValue(),nifTextField.getText());

                String name = enterpriseNameTextField.getText();
                String address = addressTextField.getText();
                String city = cityTextField.getText();
                String province = provinceTextField.getText();
                String country = countryChoiceBox.getValue();
                String locale = localeChoiceBox.getValue();
                String nif = nifTextField.getText();

                if (validator.validateString(name,1,45) && validator.validateString(address,1,45) && validator.validateString(city,1,45) && validator.validateString(province,1,45) && validator.validateString(country,1,2) && validator.validateString(locale,1,5) && validator.validateNif(nif)){
                    dbEnterpriseRepository.insertAlex(enterprise);
                    System.out.println("INSERT DONE");
                    idTextField.setText(enterprise.getId().toString());
                }
                if (!validator.validateString(name,1,45)){
                    enterpriseNameTextField.setError("introduce el nombre bien");
                }
                if (!validator.validateString(address,1,45)){
                    addressTextField.setError("Introduce bien el address");
                }
                if (!validator.validateString(city,1,45)){
                    cityTextField.setError("Introduce bien la city");
                }
                if (!validator.validateString(province,1,45)){
                    provinceTextField.setError("Introduce bien la province");
                }
                if (!validator.validateNif(nif)){
                    nifTextField.setError("Introduce un nif correcto");
                }



                System.out.println(enterprise);

            }

        }
    }
}
