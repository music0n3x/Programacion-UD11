package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;


public class UserListController {

    @FXML
    TableView userList;

    @FXML
    Label idLabel;

    @FXML
    Label firstNameLabel;

    @FXML
    Label lastNameLabel;

    @FXML
    Label phoneNumberLabel;

    @FXML
    Label lastLoginLabel;

    @FXML
    Label activeLabel;

    @FXML
    Label birthdayLabel;


}
